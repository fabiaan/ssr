const express = require('express')
const router = express.Router()

const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage('./scratch')

const { tutoring } = require('../logic')

router.get('/p/tutorias/recibidas', async(req, res, next)=>{
  const token = localStorage.getItem('token')
  try {
    // pendientes
    const pending = await tutoring.recibidasPendientesProfesor(token)
    // aceptadas
    const acepted = await tutoring.recibidasAceptadasProfesor(token)
    // rechazadas
    const canceled = await tutoring.recibidasCanceladasProfesor(token)
    res.render('received', { pending, acepted, canceled })
  }catch(e){
    console.error(e)
  }
})

router.get('/p/tutorias/recibidas', async(req, res, next)=>{
  const token = localStorage.getItem('token')
  try {
    // pendientes
    const pending = await tutoring.recibidasPendientesProfesor(token)
    // aceptadas
    const acepted = await tutoring.recibidasAceptadasProfesor(token)
    // rechazadas
    const canceled = await tutoring.recibidasCanceladasProfesor(token)
    res.render('received', { pending, acepted, canceled })
  }catch(e){
    console.error(e)
  }
})

router.get('/p/tutorias/aceptar/:id', async(req, res, next)=>{
  const Id = req.params.id
  const body = { state: 'Aceptada'}
  const token = localStorage.getItem('token')
  try {
    const response = await tutoring.updateTutoring(Id, body, token)
    res.redirect('/p/tutorias/recibidas')
  }catch(e){
    console.error(e)
  }
})

router.get('/p/tutorias/rechazar/:id', async(req, res, next)=>{
  const Id = req.params.id
  const body = { state: 'Rechazada'}
  const token = localStorage.getItem('token')
  try {
    const response = await tutoring.updateTutoring(Id, body, token)
    res.redirect('/p/tutorias/recibidas')
  }catch(e){
    console.log(e)
  }
})

module.exports = router
