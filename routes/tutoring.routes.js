const express = require('express')
const router = express.Router()

const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage('./scratch')

const { subject, tutoring } = require('../logic')

router.get('/p/tutorias/:id', async(req, res, next)=>{
  const { id } = req.params
  try {
    const response = await subject.getStudents(id)
    res.render('tutoring', { tutoring: response})
  }catch(e) {
    console.error(e)
  }
})

router.post('/p/tutorias/crear', async(req, res, next)=>{
  const token = localStorage.getItem('token')
  let body = {
    date: req.body.date,
    topic: req.body.topic,
    description: req.body.description,
    subject: req.body._id,
    comunication: req.body.comunication,
    software: req.body.software,
    type: req.body.type,
    assistants: req.body.assistants
  }
  try {
    const response = await tutoring.createTutoring(body, token)
    res.redirect('/p/tutorias/enviadas')
  }catch(e){
    console.log(e)
  }
})

module.exports = router
