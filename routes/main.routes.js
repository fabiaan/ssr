const express = require('express')
const router = express.Router()

const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage('./scratch')

const { subject } = require('../logic')

router.get('/p/inicio', async(req, res, next)=>{
  const token = localStorage.getItem('token')
  try {
    const response = await subject.getSubjectsTeacher(token)
    res.render('main', { subject: response })
  } catch(e){
    console.error(e)
  }
})

module.exports = router
