module.exports = {
  indexRoutes: require('./index.routes'),
  mainRoutes: require('./main.routes'),
  sendedRoutes: require('./sended.routes'),
  receivedRoutes: require('./received.routes'),
  tutoringRoutes: require('./tutoring.routes')
}