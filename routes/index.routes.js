const express = require('express')
const router = express.Router()

const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage('./scratch')

const { auth } = require('../logic')

router.get('/', (req, res, next)=>{
  res.render('index')
})

router.post('/', async(req, res, next)=>{
  let user = {
    email: req.body.email,
    password: req.body.password
  }
  try {
    const response = await auth.login(user)
    await localStorage.setItem('token', response.token)
    res.redirect('/p/inicio')
  } catch (e) {
    console.error(e)
  }
})

module.exports = router