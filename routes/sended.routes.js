const express = require('express')
const router = express.Router()

const LocalStorage = require('node-localstorage').LocalStorage
let localStorage = new LocalStorage('./scratch')

const { tutoring } = require('../logic')

router.get('/p/tutorias/enviadas', async(req, res, next)=>{
  const token = localStorage.getItem('token')
  try {
    // pendientes
    const pending = await tutoring.enviadasPendientes(token)
    // aceptadas
    const acepted = await tutoring.enviadasAceptadas(token)
    // rechazadas
    const canceled = await tutoring.enviadasRechazadas(token)
    res.render('sended', { pending, acepted, canceled })
  }catch(e){
    console.error(e)
  }
})

module.exports = router
