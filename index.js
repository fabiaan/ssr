const app = require('./config')

// port
const PORT = process.env.PORT || 3010

// Servidor
app.listen(PORT, ()=>{
  console.log(`Servidor funcionando en el puerto ${PORT}`)
})