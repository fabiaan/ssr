FROM node
COPY . /var/www
WORKDIR /var/www
RUN npm install
ENV PORT 5050
EXPOSE 5050
ENTRYPOINT [ "npm", "start" ]