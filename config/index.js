const express = require('express')
const hbs = require('hbs')
const path = require('path')

// routes
const {
  indexRoutes,
  mainRoutes,
  sendedRoutes,
  receivedRoutes,
  tutoringRoutes
} = require('../routes')

const router = express.Router()

router
  .use(express.json())
  .use(express.urlencoded({ extended: false }))

router
  .use(indexRoutes)
  .use(mainRoutes)
  .use(sendedRoutes)
  .use(receivedRoutes)
  .use(tutoringRoutes)

// app
const app = express()

// view engine setup
const pathViews = path.join(__dirname, '../views')
const pathPartials = path.join(__dirname, '../views/partials')

hbs.registerPartials(pathPartials)
app.set('views', pathViews)
app.set('view engine', 'hbs')

// static files
const pathStatic = path.join(__dirname, '../public')
app.use(express.static(pathStatic))

app.use(router)
module.exports = app