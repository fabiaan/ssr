const fetch = require('node-fetch')

const tutoring = {}

tutoring.createTutoring = async function(body, token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring`
  try {
    const res = await fetch(API, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },
      body: JSON.stringify(body)
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.updateTutoring = async function(Id, body, token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/${Id}`
  try {
    const res = await fetch(API, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      },
      body: JSON.stringify(body)
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.enviadasPendientes = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/pending`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.enviadasAceptadas = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/acepted`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.enviadasRechazadas = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/canceled`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.recibidasPendientesProfesor = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/t/received/pending`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.recibidasCanceladasProfesor = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/t/received/canceled`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

tutoring.recibidasAceptadasProfesor = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/tutoring/t/received/acepted`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  }catch(e){
    console.error(e)
  }
}

module.exports = tutoring