module.exports = {
  auth: require('./auth.logic'),
  subject: require('./subject.logic'),
  tutoring: require('./tutoring.logic')
}