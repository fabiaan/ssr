const fetch = require('node-fetch')

const auth = {}

auth.login = async function(user){
  const URL = 'http://13.85.41.121:8080'
  let API = [`${URL}/v1/api/user/verify`,`${URL}/v1/api/auth/signin`]
  try {
    const verifyUser = await fetch(API[0], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(user)
    })
    const res = await verifyUser.json()
    const tokenUser = await fetch(API[1], {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(res)
      
    })
    const response = await tokenUser.json()
    // console.log(response)
    return response
  } catch (e) {
    console.log(e)
  }
}

module.exports = auth