const fetch = require('node-fetch')

const subject = {}

subject.getSubjectsTeacher = async function(token){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/subject/teacher`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'x-access-token': token
      }
    })
    const resJSON = await res.json()
    return resJSON
  } catch (e) {
    console.log(e)
  }
}

subject.getStudents = async function(id){
  const URL = 'http://13.85.41.121:8080'
  const API = `${URL}/v1/api/subject/${id}`
  try {
    const res = await fetch(API, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    const resJSON = await res.json()
    return resJSON
  } catch (e) {
    console.log(e)
  }
}

module.exports = subject